package com.platform.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableConfigServer
public class PlatformConfigserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlatformConfigserverApplication.class, args);
	}

}
@RestController
class HelloController{
	
	@GetMapping("/")
	public String getTest() {
		return "this works";
	}
}